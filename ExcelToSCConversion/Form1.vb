﻿Imports System.Configuration
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Office.Interop
Imports System.Xml.Linq
Imports System.Text
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common

Public Class Form1

    Private Sub cmdOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOpen.Click
        Dim myStream As Stream = Nothing
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "xls files (*.xls)|*.xls|All files (*.*)|*.*"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                txtInfile.Text = openFileDialog1.FileName
            Catch Ex As Exception
                MessageBox.Show("Cannot read file from disk. Original error: " & Ex.Message)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.
            End Try
        End If
    End Sub

    Private Sub cmdConvert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConvert.Click

        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim lFileNumber As Long
        Dim lTmpFileNumber As Long
        Dim lNdx As Long
        Dim bFirst As Boolean
        Dim oConn As SqlConnection
        Dim oCmd As SqlCommand
        Dim oReader As SqlDataReader
        Dim oCmd2 As SqlCommand
        Dim oReader2 As SqlDataReader
        Dim sFileNum As String
        Dim sInitDate As String
        Dim sEffectiveDate As String
        Dim txtVendorName As String
        Dim sPayee As String
        Dim sVendID As String
        Dim dTotalInvoiceAmount As Double
        Dim sInvoiceNbr As String
        Dim lClientID As Integer

        Try

            If rbXMLOutputExpenses.Checked = False And rbXMLOutputInvoices.Checked = False Then
                MessageBox.Show("Please select an XML output format.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Try
            End If

            'Dim doc As New XDocument
            'doc.Add(New XDeclaration("1.0", "", ""))
            'doc.Add(New XDocumentType("import.dtd", "importfile", "system", ""))
            'oConn = New SqlConnection("MultipleActiveResultSets=True;server=65.182.201.198;uid=sa;password=shalom5723;initial catalog=TPA2001_10_2_14_59_37;Connect Timeout=200")
            oConn = New SqlConnection("MultipleActiveResultSets=True;server=65.182.211.150;uid=rvc;password=Sh@l0m5723;initial catalog=st_carmax;Connect Timeout=200")
            'oConn = New SqlConnection("MultipleActiveResultSets=True;server=10.0.0.39;uid=sa;password=Pr0beach;initial catalog=st_carmax;Connect Timeout=200")
            'oConn = New SqlConnection(ConfigurationSettings.AppSettings("ExcelToRVConversion.My.MySettings.stConnectionString"))
            'oConn = New SqlConnection(configurationmanager.AppSettings("ExcelToRVConversion.My.MySettings.stConnectionString"))
            oConn.Open()

            ' create the new file to write the xml to
            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "", False)

            lblProgress.Visible = True
            pbProgress.Visible = True
            pbProgress.Value = 0
            txtInfile.Enabled = False
            txtOutputFile.Enabled = False
            cmdConvert.Enabled = False
            cmdOpen.Enabled = False
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<?xml version=""1.0"" ?>" & vbCrLf, False)
            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<!DOCTYPE importfile SYSTEM ""import.dtd"" >" & vbCrLf, True)
            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, String.Concat("<importfile importdate=""", FormatDateTime(Now, 2), """ >") & vbCrLf, True)

            'If lbClient.SelectedValue > 0 Then
            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, String.Concat("<company name=""", lbClient.Text, """ >") & vbCrLf, True)
            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<configuration>" & vbCrLf, True)
            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<uri urifield=""TAXID""/>" & vbCrLf, True)

            ' add user defined fields for the expense entries being added
            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<userfields>" & vbCrLf, True)
            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<userfield name=""PAIDINCURRENCY"" type = ""TEXT"" required=""N"" page=""EXPENSE"" />" & vbCrLf, True)
            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<userfield name=""PAIDAMOUNT"" type = ""MONEY"" required=""N"" page=""EXPENSE"" />" & vbCrLf, True)
            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<userfield name=""USDFX"" type = ""TEXT"" required=""N"" page=""EXPENSE"" />" & vbCrLf, True)
            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</userfields>" & vbCrLf, True)

            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</configuration>" & vbCrLf, True)

            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<employees mergeemployees=""Y"">" & vbCrLf, True)

            ' loop through the passed in excel file and export all expenses to the xml file
            xlApp = New Excel.ApplicationClass
            xlWorkBook = xlApp.Workbooks.Open(txtInfile.Text)
            'xlWorkBook = xlApp.Workbooks.Open("c:\Malaysia_Jan to Aug08_Import.xls")
            xlWorkSheet = xlWorkBook.Worksheets(1)

            lNdx = 3
            While Len(xlWorkSheet.Cells(lNdx, 1).value) <> 0
                dTotalInvoiceAmount += xlWorkSheet.Cells(lNdx, 10).value
                lNdx += 1
            End While
            sInvoiceNbr = Format(Now, "MM/dd/yyyy hh:mm:ss")

            lNdx = 3
            lFileNumber = xlWorkSheet.Cells(lNdx, 1).value
            lTmpFileNumber = 0
            bFirst = True
            'While Trim("" & IIf(IsNothing(xlWorkSheet.Cells(lNdx, 6).value.ToString), "", xlWorkSheet.Cells(lNdx, 6).value.ToString)) <> ""
            While Len(xlWorkSheet.Cells(lNdx, 1).value) <> 0
                lFileNumber = xlWorkSheet.Cells(lNdx, 1).value
                If lTmpFileNumber <> lFileNumber Then
                    lTmpFileNumber = xlWorkSheet.Cells(lNdx, 1).value
                    If bFirst Then
                        bFirst = False
                        'get the client info
                        oCmd2 = New SqlCommand("svc_GetClientByTransferID", oConn)
                        oCmd2.CommandType = CommandType.StoredProcedure
                        oCmd2.Parameters.Add(New SqlParameter("@RelocateeID", SqlDbType.Int)).Value = lFileNumber
                        oReader2 = oCmd2.ExecuteReader(CommandBehavior.Default)
                        If oReader2.Read Then

                            lClientID = oReader2("ClientID")
                            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, String.Concat("<company name=""", FixNonStandardASC(Trim(oReader2("Company"))), """ >") & vbCrLf, True)
                            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<configuration>" & vbCrLf, True)
                            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<uri urifield=""TRANSFERID""/>" & vbCrLf, True)
                            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<userfields>" & vbCrLf, True)
                            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<companyuserfield name=""Invoice Received"" type = ""DATE"" required=""N"" page=""EXPENSE"" />" & vbCrLf, True)
                            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<companyuserfield name=""Expense Acct Number"" type = ""TEXT"" required=""N"" page=""EXPENSE"" />" & vbCrLf, True)
                            'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</userfields>" & vbCrLf, True)
                            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</configuration>" & vbCrLf, True)

                            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<employees mergeemployees=""Y"">" & vbCrLf, True)

                        End If
                        oReader2.Close()
                        oCmd2.Dispose()
                    Else
                        My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</expenses>" & vbCrLf, True)
                        My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</employee>" & vbCrLf, True)
                        'get the client info
                        oCmd2 = New SqlCommand("svc_GetClientByTransferID", oConn)
                        oCmd2.CommandType = CommandType.StoredProcedure
                        oCmd2.Parameters.Add(New SqlParameter("@RelocateeID", SqlDbType.Int)).Value = lFileNumber
                        oReader2 = oCmd2.ExecuteReader(CommandBehavior.Default)
                        If oReader2.Read Then
                            If lClientID <> Val(oReader2("ClientID")) Then
                                My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</employees>" & vbCrLf, True)
                                My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</company>" & vbCrLf, True)

                                'get the client info
                                oCmd2 = New SqlCommand("svc_GetClientByTransferID", oConn)
                                oCmd2.CommandType = CommandType.StoredProcedure
                                oCmd2.Parameters.Add(New SqlParameter("@RelocateeID", SqlDbType.Int)).Value = lFileNumber
                                oReader2 = oCmd2.ExecuteReader(CommandBehavior.Default)
                                If oReader2.Read Then

                                    lClientID = oReader2("ClientID")

                                    My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, String.Concat("<company name=""", FixNonStandardASC(Trim(oReader2("Company"))), """ >") & vbCrLf, True)
                                    My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<configuration>" & vbCrLf, True)
                                    My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<uri urifield=""TRANSFERID""/>" & vbCrLf, True)
                                    'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<userfields>" & vbCrLf, True)
                                    'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<companyuserfield name=""Invoice Received"" type = ""DATE"" required=""N"" page=""EXPENSE"" />" & vbCrLf, True)
                                    'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<companyuserfield name=""Expense Acct Number"" type = ""TEXT"" required=""N"" page=""EXPENSE"" />" & vbCrLf, True)
                                    'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</userfields>" & vbCrLf, True)
                                    My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</configuration>" & vbCrLf, True)

                                    My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<employees mergeemployees=""Y"">" & vbCrLf, True)

                                End If
                            End If
                        End If
                        oReader2.Close()
                        oCmd2.Dispose()
                    End If
                    ' get the new relocatee record
                    oCmd = New SqlCommand("usp_Sel_Relocatee", oConn)
                    oCmd.CommandType = CommandType.StoredProcedure
                    oCmd.Parameters.Add(New SqlParameter("@iRelocateeID", SqlDbType.Int)).Value = lFileNumber
                    oReader = oCmd.ExecuteReader(CommandBehavior.Default)
                    If oReader.Read Then
                        '<employee firstname="Christopher" lastname="Horn" taxid="SC6479" scid="6479" initdate="04/07/2008" employeenumber="" filenumber="111111111" effectivedate="09/17/2007" movetype="Domestic - Full Relocation" movereason="Domestic - Permanent" >
                        ' start a new employee record
                        If Year(IIf(IsDBNull(oReader("jobstartdate")), "12/30/1899", oReader("jobstartdate"))) <> 1899 Then
                            sEffectiveDate = Format(oReader("jobstartdate"), "MM/dd/yyyy")
                        Else
                            sEffectiveDate = Format(oReader("EnteredDate"), "MM/dd/yyyy")
                        End If

                        If Year(IIf(IsDBNull(oReader("InitiationDate")), "12/30/1899", oReader("InitiationDate"))) <> 1899 Then
                            sInitDate = Format(oReader("InitiationDate"), "MM/dd/yyyy")
                        Else
                            sInitDate = Format(oReader("EnteredDate"), "MM/dd/yyyy")
                        End If
                        My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<employee firstname=""" & FixNonStandardASC(Trim(oReader("FirstName"))) & """ lastname=""" & FixNonStandardASC(Trim(oReader("LastName"))) & """ taxid="""" transferid=""" & lFileNumber & """ initdate=""" & sInitDate & """ effectivedate=""" & sEffectiveDate & """ >" & vbCrLf, True)
                    End If
                    oReader.Close()
                    oCmd.Dispose()
                    My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<expenses>" & vbCrLf, True)
                End If

                ' get the new relocatee record
                'If LCase(Mid(xlWorkSheet.Cells(lNdx, 7).value, 1, 3)) = "scv" Then
                '    lVendID = Mid(xlWorkSheet.Cells(lNdx, 7).value, 4)
                'Else
                '    lVendID = xlWorkSheet.Cells(lNdx, 7).value
                'End If
                'If IsNumeric(lVendID) Then
                '    oCmd = New SqlCommand("svc_GetVendor", oConn)
                '    oCmd.CommandType = CommandType.StoredProcedure
                '    oCmd.Parameters.Add(New SqlParameter("@VendorID", SqlDbType.Int)).Value = lVendID
                '    oReader = oCmd.ExecuteReader(CommandBehavior.Default)
                '    txtVendorName = ""
                '    If oReader.Read Then
                '        txtVendorName = Trim(oReader("VendorName"))
                '    End If
                '    oReader.Close()
                '    oCmd.Dispose()
                'Else
                '    txtVendorName = xlWorkSheet.Cells(lNdx, 7).value
                'End If
                txtVendorName = ""

                sPayee = String.Empty
                sVendID = String.Empty
                If xlWorkSheet.Cells(lNdx, 12).value = "Employee" Then
                    sPayee = "Employee"
                Else
                    sPayee = "O"
                    sVendID = "payeeid = """ & xlWorkSheet.Cells(lNdx, 13).value & """"
                End If

                My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<expense date=""" & Format(xlWorkSheet.Cells(lNdx, 14).value, "MM/dd/yyyy") & """ code=""" & xlWorkSheet.Cells(lNdx, 11).value & """ amountusd=""" & xlWorkSheet.Cells(lNdx, 10).value & """ amountforeign=""" & xlWorkSheet.Cells(lNdx, 10).value & """ amountexchangerate=""1"" amountcurrencycode=""USD"" description=""" & FixNonStandardASC(xlWorkSheet.Cells(lNdx, 9).value) & """ reportdate=""" & Format(xlWorkSheet.Cells(lNdx, 4).value, "MM/dd/yyyy") & """ payee=""" & sPayee & """ " & sVendID & " apdisbursement = """ & xlWorkSheet.Cells(lNdx, 16).value & """>" & vbCrLf, True)
                'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<expenseuserfields>" & vbCrLf, True)
                'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<expenseuserfielddata name=""Invoice Received"" value=""" & Trim(xlWorkSheet.Cells(lNdx, 11).value) & """ />" & vbCrLf, True)
                'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<expenseuserfielddata name=""Expense Acct Number"" value=""" & xlWorkSheet.Cells(lNdx, 5).value & """ />" & vbCrLf, True)
                'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<expenseuserfielddata name=""USDFX"" value=""" & xlWorkSheet.Cells(lNdx, 7).value & """ />" & vbCrLf, True)
                'My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</expenseuserfields>" & vbCrLf, True)
                ' add an invoice record for each expense.   have to generate a custom invoice number

                My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<expenseuserfields>" & vbCrLf, True)
                My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<expenseuserfielddata name=""Expense Incurred Date"" value= """ & Format(xlWorkSheet.Cells(lNdx, 7).value, "MM/dd/yyyy") & """/>" & vbCrLf, True)
                My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</expenseuserfields>" & vbCrLf, True)

                If rbXMLOutputInvoices.Checked = True Then
                    My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "<parentinvoice invoicenumber=""" & sInvoiceNbr & """ invoicetotal=""" & dTotalInvoiceAmount & """ description=""" & FixNonStandardASC(xlWorkSheet.Cells(lNdx, 9).value) & """ autocreate=""Y"" />" & vbCrLf, True)
                End If
                My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</expense>" & vbCrLf, True)

                lNdx += 1

                If lNdx <= 1000 Then pbProgress.Value = lNdx
                System.Windows.Forms.Application.DoEvents()
            End While

            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)

            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</expenses>" & vbCrLf, True)
            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</employee>" & vbCrLf, True)
            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</employees>" & vbCrLf, True)
            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</company>" & vbCrLf, True)
            My.Computer.FileSystem.WriteAllText(txtOutputFile.Text, "</importfile>" & vbCrLf, True)
            'End If

            'var(bytes = System.IO.File.ReadAllBytes(txtOutputFile.Text))
            'System.IO.File.WriteAllBytes(Name, bytes.Skip(3).ToArray())
            File.WriteAllText(txtOutputFile.Text, File.ReadAllText(txtOutputFile.Text, Encoding.UTF8), Encoding.ASCII)

            lblProgress.Visible = False
            pbProgress.Visible = False
            txtInfile.Enabled = True
            txtOutputFile.Enabled = True
            cmdConvert.Enabled = True
            cmdOpen.Enabled = True
            System.Windows.Forms.Cursor.Current = Cursors.Default
            MessageBox.Show("File Created Successfully!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.None)

        Catch ex As Exception
            MessageBox.Show("File Creation Failed:" & ex.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'StDataSet1.NSP_Sel_GetAllClient' table. You can move, or remove it, as needed.
        'Me.NSP_Sel_GetAllClientTableAdapter.Fill(Me.StDataSet1.NSP_Sel_GetAllClient)

        txtOutputFile.Text = "c:\rvimports\Expense_Import_" & Format(Now, "yyyyMMdd_hhmm") & ".xml"

    End Sub

    ' Retrieves a connection string by name.
    ' Returns Nothing if the name is not found.
    '    Private Shared Function GetConnectionStringByName( _
    '       ByVal name As String) As String

    ' Assume failure
    '  Dim returnValue As String = Nothing
    ' Dim settings As ConnectionStringSettings
    '
    ' Look for the name in the connectionStrings section.
    '   settings = ConfigurationManager.ConnectionStrings(name)

    ' If found, return the connection string.
    '  If Not settings Is Nothing Then
    '     returnValue = settings.ConnectionString
    'End If
    '
    '   Return returnValue
    'End Function

    Public Function FixNonStandardASC(ByVal sOldString As Object) As String
        'mjs 06/13/2006
        Dim idx As Long
        Dim lCount As Long

        sOldString = Replace(sOldString, "&", "&amp;")
        sOldString = Replace(sOldString, "<", "&lt;")
        sOldString = Replace(sOldString, ">", "&gt;")
        sOldString = Replace(sOldString, """", "&quot;")

        lCount = Len(sOldString)
        For idx = 1 To lCount
            If (Asc(Microsoft.VisualBasic.Strings.Mid(sOldString, idx, 1)) >= 127 Or Asc(Microsoft.VisualBasic.Strings.Mid(sOldString, idx, 1)) <= 31) Then
                sOldString = Microsoft.VisualBasic.Strings.Left(sOldString, idx - 1) & " " & Microsoft.VisualBasic.Strings.Mid(sOldString, idx + 1)
            End If
        Next

        FixNonStandardASC = Trim(sOldString)

    End Function

End Class
