Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Array

Namespace Orion.BusinessLayer
    Class OrionEnum
        Public Enum UserSelection
            [All]
            Users
            ClientUsers
        End Enum
    End Class

    Public Enum Status
        Inactive = 0
        [Active] = 1
        All = 2
    End Enum

    Public Enum UserStatus
        Expired = 2
        Active = 1
        [All] = 0
    End Enum

    Public Enum CostofLivingType
        Assistance
        Multiplier
        Others
    End Enum
    Public Enum CommunicationMethods
        Fax
        Phone
        eMail
        Mail
    End Enum
    Public Enum RowsPerList
        Rows = 20
    End Enum
    Public Enum SortingOrder
        Asc
        Desc
    End Enum
    Public Enum RowsPerVendor
        Rows = 10
    End Enum
    Public Enum ServiceTypes
        AC
        International
        Domestic
    End Enum
    Public Enum SystemsValue
        SysValue = 0
    End Enum
    Public Enum CountryCode
        USA
    End Enum
    Public Enum AddressTypes
        OriginResident = 1
        OriginOffice = 2
        Interim = 3
        DestinationResident = 4
        DestinationOffice = 5
    End Enum

    Public Enum TableType
        CL  'Client
        RS  'Referral Source
        CN  'Global Contact
        TR  'Transferee
        VN  'Vendor
        RL  'Relocatee
        RD  'Relocatee Dependent
        VC  'Vendor Contact
        CV  'Client Vendor
        CR  'Client Report
        CU  'Client User
        VS  'Vendor Service
    End Enum
    Public Enum Direction
        [In]
        Out
    End Enum
    Public Enum ButtonCaption
        Add
        Update
        Cancel
        Clear
        Close
        Duplicate
        Save
    End Enum
    Public Enum MessageType
        Client
        Vendor
        Transferee
    End Enum
    Public Enum WeeKday
        Sunday
        Monday
        Tuesday
        Wednesday
        Thursday
        Friday
        Saturday
    End Enum
    Public Enum Coordinator
        Both
        Origin
        Destination

    End Enum

    Public Enum Month
        January
        February
        March
        April
        May
        June
        July
        August
        September
        October
        November
        December
    End Enum
    Public Enum VendorSort
        Vendorname
        ServiceType
        StateCity
    End Enum
    Public Enum ReportsSchedule
        Annually
        Monthly
        Quarterly
        Weekly
    End Enum
    Public Enum ReferenceMasterTable
        Accommodation
        CommissionType
        AccountType
        AddressType
        eMailType
        MoveReason
        MoveType
        PhoneType
        PreferredBillingType
        PreferredCommType
        FormType
        ref_PropertyType
        ReferralRateType
        ReferralTerm
        ReferralType
        Salutation
        ProDesignation
        Unit
        ref_InvoiceDescription
        ref_RequiredForm
        ref_ContractType
    End Enum
    Public Enum VendorRating
        Excellent
        Good
        Average
        Poor
    End Enum
    Public Enum OwnerType
        Consumer
        PMC
    End Enum
    Public Enum TypeStatus
        [New]
        Delete
    End Enum
End Namespace
