Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Security
Imports System.Security.Cryptography
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.IO

Namespace OrionUtility
    Public Class DBUtility
#Region "Methods to validate entry to Parameters"
        ''' <summary>
        ''' To check whether date value is null.
        ''' </summary>
        ''' <param name="val"></param>
        ''' <returns>Either date or null value </returns>
        ''' <remarks></remarks>
        Public Shared Function ParamDateTime(ByVal val As DateTime) As Object
            If val = DateTime.MinValue Then
                Return Convert.ToDateTime("12/30/1899")
            Else
                Return val
            End If
        End Function
        ''' <summary>
        ''' To check whether parameter value contains decimal part.
        ''' </summary>
        ''' <param name="val"></param>
        ''' <returns>Either decimal value or null value </returns>
        ''' <remarks></remarks>
        Public Shared Function ParamDecimal(ByVal val As Decimal) As Object
            If val = Decimal.MinValue Then
                Return DBNull.Value
            Else
                Return val
            End If
        End Function
        ''' <summary>
        ''' To check whether parameter value contains integer part.
        ''' </summary>
        ''' <param name="val"></param>
        ''' <returns>Either integer value or null value </returns>
        ''' <remarks></remarks>

        Public Shared Function ParamInt(ByVal val As Integer) As Object
            If val = Integer.MinValue Then
                Return DBNull.Value
            Else
                Return val
            End If
        End Function
        ''' <summary>
        ''' To check whether parameter value contains integer(small int) part.
        ''' </summary>
        ''' <param name="val"></param>
        ''' <returns>Either integer(small int) value or null value </returns>
        ''' <remarks></remarks>
        Public Shared Function ParamSmallInt(ByVal val As Int16) As Object
            If val = Int16.MinValue Then
                Return DBNull.Value
            Else
                Return val
            End If
        End Function

        ''' <summary>
        ''' To check whether parameter value contains string.
        ''' </summary>
        ''' <param name="val"></param>
        ''' <returns>Either string value or null value </returns>
        ''' <remarks></remarks>
        Public Shared Function ParamString(ByVal val As String) As Object
            If val = String.Empty Then
                Return DBNull.Value
            Else
                Return val
                'Return val.Replace("'", "''")
            End If
        End Function

        ''' <summary>
        ''' To check whether parameter value contains boolean value or not.
        ''' </summary>
        ''' <param name="val"></param>
        ''' <returns>Either boolean value or null value </returns>
        ''' <remarks></remarks>
        Public Shared Function ParamBoolean(ByVal val As Boolean) As Object
            If val = True Then
                Return True
            ElseIf val = False Then
                Return False
            Else
                Return DBNull.Value
            End If
        End Function
#End Region

#Region "Methods to validate values retrieved from Database"
        ''' <summary>
        ''' To check whether parameter contains null value or date value.
        ''' </summary>
        ''' <param name="val">Value as Object</param>
        ''' <returns>Either actual date(parameter value) or default date instead of null </returns>
        ''' <remarks></remarks>

        Public Shared Function ValidateDateTime(ByVal val As Object) As DateTime
            Dim retVal As DateTime

            If val Is DBNull.Value Then
                retVal = DateTime.MinValue
            Else
                '    Dim format As IFormatProvider = New System.Globalization.CultureInfo("en-GB", True)
                Dim format As IFormatProvider = New System.Globalization.CultureInfo("en-US", True)
                retVal = Convert.ToDateTime(val, format)
            End If

            Return retVal
        End Function

        ''' <summary>
        ''' To check whether parameter contains null value or integer value.
        ''' </summary>
        ''' <param name="val">Value as Object</param>
        ''' <returns>Either actual integer or null value</returns>
        ''' <remarks></remarks>
        Public Shared Function ValidateInt(ByVal val As Object) As Integer
            Dim retVal As Integer

            If val Is DBNull.Value Then
                retVal = Integer.MinValue
            Else
                retVal = Convert.ToInt32(val)
            End If

            Return retVal
        End Function
        ''' <summary>
        ''' To check whether parameter contains null value or integer value.
        ''' </summary>
        ''' <param name="val">Value as Object</param>
        ''' <returns>Either actual integer or default integer instead of null </returns>
        ''' <remarks></remarks>

        Public Shared Function ValidateSmallInt(ByVal val As Object) As Int16
            Dim retVal As Int16

            If val Is DBNull.Value Then
                retVal = Int16.MinValue
            Else
                retVal = Convert.ToInt16(val)
            End If

            Return retVal
        End Function
        ''' <summary>
        ''' To check whether parameter contains null value or char value.
        ''' </summary>
        ''' <param name="val">Value as Object</param>
        ''' <returns>Either actual char or default char instead of null </returns>
        ''' <remarks></remarks>

        Public Shared Function ValidateChar(ByVal val As Object) As Char
            Dim retVal As Char

            If val Is DBNull.Value Then
                retVal = Char.MinValue
            Else
                retVal = Convert.ToChar(val)
            End If

            Return retVal
        End Function
        ''' <summary>
        ''' To check whether parameter contains null value or string value.
        ''' </summary>
        ''' <param name="val">Value as Object</param>
        ''' <returns>Either actual string or empty value instead of null </returns>
        ''' <remarks></remarks>

        Public Shared Function ValidateString(ByVal val As Object) As String
            Dim retVal As String

            If val Is DBNull.Value Then
                retVal = String.Empty
            Else
                retVal = Convert.ToString(val).Trim()
            End If

            Return retVal
        End Function
        ''' <summary>
        ''' To check whether parameter contains null value or decimal value.
        ''' </summary>
        ''' <param name="val">Value as Object</param>
        ''' <returns>Either actual decimal or default decimal value instead of null </returns>
        ''' <remarks></remarks>
        Public Shared Function ValidateDecimal(ByVal val As Object) As Decimal
            Dim retVal As Decimal

            If val Is DBNull.Value Then
                retVal = Decimal.Zero
            Else
                retVal = Convert.ToDecimal(val)
            End If

            Return retVal
        End Function
        ''' <summary>
        ''' To check whether parameter contains null value or integer value.
        ''' </summary>
        ''' <param name="val">Value as Object</param>
        ''' <returns>Either integer or default integer instead of null </returns>
        ''' <remarks></remarks>

        Public Shared Function ValidateFloat(ByVal val As Object) As Single
            Dim retVal As Single

            If val Is DBNull.Value Then
                retVal = Single.MinValue

            Else
                retVal = Convert.ToSingle(val)
            End If

            Return retVal
        End Function
        ''' <summary>
        ''' To check whether parameter contains boolean value.
        ''' </summary>
        ''' <param name="val">Value as Object</param>
        ''' <returns>Boolean value </returns>
        ''' <remarks></remarks>

        Public Shared Function ValidateBoolean(ByVal val As Object) As Boolean
            Dim retVal As Boolean

            If val Is DBNull.Value Then
                retVal = False
            Else
                retVal = Convert.ToBoolean(val)
            End If

            Return retVal
        End Function

#End Region
#Region "Methods to validate userid and password to avoid sql injection attacks"
        ''' <summary>
        ''' To check whether parameter contains empty string.
        ''' </summary>
        ''' <param name="val">String value</param>
        ''' <returns>String</returns>
        ''' <remarks></remarks>

        Public Shared Function ValidateUserIDPasswordInput(ByVal val As String) As String
            If val = String.Empty Then
                Return ""
            Else
                val = val.Replace("--", "")
                Return val.Replace("'", "''")
            End If
        End Function
#End Region

#Region "Get Transaction - This function has to be invoked in-order to get a DbTransaction object."
        ''' <summary>
        ''' To create database connection
        ''' </summary>
        ''' <returns>Transaction object</returns>
        ''' <remarks></remarks>

        Public Shared Function GetTransaction() As DbTransaction
            Dim dbFactory As Database = DatabaseFactory.CreateDatabase()
            Dim conn As DbConnection = dbFactory.CreateConnection()
            conn.Open()
            Dim dbTransaction As DbTransaction = conn.BeginTransaction()
            Return dbTransaction
        End Function
#End Region

#Region "Encrypt the security setting algorithm"
        ''' <summary>
        ''' Entcription for user role security settings
        ''' </summary>
        ''' <param name="sWork">Decrypted bit</param>
        ''' <param name="lPos">Possition for security bit</param>
        ''' <param name="bValue"></param>
        ''' <remarks></remarks>
        Public Shared Sub SetBit(ByRef sWork As String, ByVal lPos As Long, ByVal bValue As Boolean)
            Try
                Dim idx As Long
                Dim iBit As Integer

                idx& = (lPos \ 8) + 1
                iBit = 2 ^ (lPos Mod 8)

                If Len(sWork$) < idx& Then sWork$ = Left(sWork$ + StrDup(CInt(idx& - Len(sWork$)), Chr(0)), idx&) 'Left(sWork$ + String(idx& - Len(sWork$), 0), idx&)

                If bValue = 0 Then
                    Mid$(sWork$, idx&, 1) = Chr(Asc(Mid$(sWork$, idx&, 1)) And (255 - iBit%)) 'Chr$(Asc(Mid$(sWork$, idx&, 1)) And (255 - iBit%))
                Else
                    Mid$(sWork$, idx&, 1) = Chr(Asc(Mid$(sWork$, idx&, 1)) Or iBit%) 'Chr$(Asc(Mid$(sWork$, idx&, 1)) Or iBit%)
                End If
            Catch ex As Exception
                Throw
            End Try

        End Sub


#End Region

#Region "Decrypt the security setting algorithm"
        ''' <summary>
        ''' Decription for user role security settings
        ''' </summary>
        ''' <param name="sWork">Encrypted bit</param>
        ''' <param name="lPos">Possition for security bit</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetBit(ByVal sWork As String, ByVal lPos As Long) As Boolean
            Try

                Dim idx As Long
                Dim iBit As Integer


                idx& = (lPos \ 8) + 1
                iBit = 2 ^ (lPos Mod 8)

                If Len(sWork) < idx& Then sWork = Left(sWork + StrDup(CInt(idx& - Len(sWork)), Chr(0)), idx&) 'Left$(sWork$ + String(idx& - Len(sWork$), 0), idx&)
                GetBit = Asc(Mid(sWork, idx&, 1)) And iBit%

            Catch ex As Exception
                Throw
            End Try

        End Function



#End Region



#Region "Encrypt using MD5 hashing algorithm"

        Public Const sPassword As String = "NOIRNYAWETAGSOFTTWAREISGREAT"
        ''' <summary>
        ''' Password Encription
        ''' </summary>
        ''' <param name="sWork">Data to Encrypt</param>
        ''' <param name="sPassword">String used for Encryption</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Encrypt(ByVal sWork As String, ByVal sPassword As String) As String

            Dim idx As Long
            Dim lLenPass As Long
            Dim lPass As Long

            Try
                'sWork = LCase(sWork) 'being commented to remove password case sensitivity 08-05-08
                lLenPass = Len(sPassword$)

                For idx = 1 To Len(sWork)
                    lPass& = Asc(Mid(sPassword, (idx Mod lLenPass) - lLenPass * ((idx& Mod lLenPass) = 0), 1))
                    Mid(sWork, idx&, 1) = Chr(Asc(Mid(sWork, idx&, 1)) Xor lPass&)
                Next


                Return sWork
            Catch ex As Exception
                Throw
            End Try

        End Function
        ''' <summary>
        ''' Data Encription 
        ''' </summary>
        ''' <param name="input">Data to Encrypt</param>
        ''' <param name="key">Mode of Encrypt</param>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Shared Function Encryption(ByVal input As String, ByVal key As String) As String

            Dim des As TripleDESCryptoServiceProvider = Nothing
            Dim hashmd5 As MD5CryptoServiceProvider
            Dim pwdhash
            Dim buff() As Byte
            Try
                Dim result As String
                hashmd5 = New MD5CryptoServiceProvider()
                pwdhash = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key))
                hashmd5 = Nothing
                des = New TripleDESCryptoServiceProvider()
                des.Key = pwdhash
                des.Mode = CipherMode.ECB 'CBC, CFB
                buff = ASCIIEncoding.ASCII.GetBytes(input)
                result = Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buff, 0, buff.Length))
                Return result
            Finally
                If des IsNot Nothing Then
                    des.Clear()
                    des = Nothing
                End If
            End Try
        End Function
        ''' <summary>
        ''' String Encription
        ''' </summary>
        ''' <param name="input">Input String</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Encrypt(ByVal input As String) As String
            'try
            '{
            '}
            'catch (Exception exception)
            '{
            '}
            Return String.Empty
        End Function
#End Region

#Region "Decrypt using MD5 hashing algorithm"
        ''' <summary>
        ''' Data decription
        ''' </summary>
        ''' <param name="input">Data to decrypt</param>
        ''' <param name="key">Mode of Decrpt Format</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Decrypt(ByVal input As String, ByVal key As String) As String

            Dim des As TripleDESCryptoServiceProvider = Nothing
            Dim hashmd5 As MD5CryptoServiceProvider
            Dim pwdhash
            Dim buff() As Byte
            Try
                Dim result As String
                hashmd5 = New MD5CryptoServiceProvider()
                pwdhash = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key))
                hashmd5 = Nothing
                des = New TripleDESCryptoServiceProvider()
                des.Key = pwdhash
                des.Mode = CipherMode.ECB 'CBC, CFB

                buff = Convert.FromBase64String(input)
                result = ASCIIEncoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(buff, 0, buff.Length))
                Return result
            Finally
                If des IsNot Nothing Then
                    des.Clear()
                    des = Nothing
                End If
            End Try
        End Function
        ''' <summary>
        ''' String decription
        ''' </summary>
        ''' <param name="input">Input String</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Decrypt(ByVal input As String) As String
            'try 
            '{ 
            '}
            'catch (Exception ex)
            '{ 
            '}
            Return String.Empty
        End Function
#End Region

#Region "Create a datatable based on the list of columns supplied"
        ''' <summary>
        '''     This function creates a datatable with the list of 
        '''     columnd supplied to it
        ''' </summary>
        ''' <param name="columnList">Columns details</param>
        ''' <returns> datatable with columns as per the list supplied </returns>
        Public Shared Function CreatedDataTable(ByVal columnList As List(Of String)) As DataTable
            Dim datatable As DataTable = New DataTable()

            For Each column As String In columnList
                datatable.Columns.Add(column)
            Next column

            Return datatable
        End Function
#End Region

#Region "Regular Expressions"

        ''' <summary>
        ''' Function to test for Positive Integers.  
        ''' </summary>
        ''' <param name="strNumber">Number to check</param>
        ''' <returns>whether supplied string contains regular expression or not</returns>
        ''' <remarks></remarks>
        Public Shared Function IsNaturalNumber(ByVal strNumber As String) As Boolean
            Dim notNaturalPattern As Regex = New Regex("[^0-9]")
            Dim naturalPattern As Regex = New Regex("0*[1-9][0-9]*")
            Return (Not notNaturalPattern.IsMatch(strNumber)) AndAlso naturalPattern.IsMatch(strNumber)
        End Function
        ''' <summary>
        ''' To check whether parameter value contains maximum integer value
        ''' </summary>
        ''' <param name="strNumber">Number to check</param>
        ''' <returns>Flase when supplied value is greater than maxvalue otherwise True </returns>
        ''' <remarks></remarks>
        Public Shared Function IsMaxInteger(ByVal douNumber As Double) As Boolean

            If douNumber > Integer.MaxValue Then
                Return False
            Else
                Return True
            End If

        End Function

        ''' <summary>
        ''' Function to test for Positive Integers with zero inclusive  
        ''' </summary>
        ''' <param name="strNumber">Number to check</param>
        ''' <returns>True if supplied value is whole number</returns>
        ''' <remarks></remarks>
        Public Shared Function IsWholeNumber(ByVal strNumber As String) As Boolean
            Dim notWholePattern As Regex = New Regex("[^0-9]")
            Return Not notWholePattern.IsMatch(strNumber)
        End Function
        ''' <summary>
        ''' Function to Test for Integers both Positive and Negative
        ''' </summary>
        ''' <param name="strNumber">Number to check</param>
        ''' <returns>True or False</returns>
        ''' <remarks></remarks>
        Public Shared Function IsInteger(ByVal strNumber As String) As Boolean
            If (Not strNumber Is Nothing) Then
                strNumber = strNumber.Trim()
                Dim notIntPattern As Regex = New Regex("[^0-9-]")
                Dim intPattern As Regex = New Regex("^-[0-9]+$|^[0-9]+$")
                Return (Not notIntPattern.IsMatch(strNumber)) AndAlso intPattern.IsMatch(strNumber)
            Else
                Return False
            End If
        End Function

        ''' <summary>
        ''' Function to Test for Positive Number both Integer and Real
        ''' </summary>
        ''' <param name="strNumber">Number to check</param>
        ''' <returns>True or False</returns>
        ''' <remarks></remarks>
        Public Shared Function IsPositiveNumber(ByVal strNumber As String) As Boolean
            Dim notPositivePattern As Regex = New Regex("[^0-9.]")
            Dim positivePattern As Regex = New Regex("^[.][0-9]+$|[0-9]*[.]*[0-9]+$")
            Dim twoDotPattern As Regex = New Regex("[0-9]*[.][0-9]*[.][0-9]*")
            Return (Not notPositivePattern.IsMatch(strNumber)) AndAlso positivePattern.IsMatch(strNumber) AndAlso Not twoDotPattern.IsMatch(strNumber)
        End Function
        ''' <summary>
        ''' Function to test whether the string is valid number or not
        ''' </summary>
        ''' <param name="strNumber">Number to check</param>
        ''' <returns>True or False</returns>
        ''' <remarks></remarks>
        Public Shared Function IsNumber(ByVal strNumber As String) As Boolean
            Dim notNumberPattern As Regex = New Regex("[^0-9.-]")
            Dim twoDotPattern As Regex = New Regex("[0-9]*[.][0-9]*[.][0-9]*")
            Dim twoMinusPattern As Regex = New Regex("[0-9]*[-][0-9]*[-][0-9]*")
            Dim strValidRealPattern As String = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$"
            Dim strValidIntegerPattern As String = "^([-]|[0-9])[0-9]*$"
            Dim numberPattern As Regex = New Regex("(" & strValidRealPattern & ")|(" & strValidIntegerPattern & ")")
            Return (Not notNumberPattern.IsMatch(strNumber)) AndAlso (Not twoDotPattern.IsMatch(strNumber)) AndAlso (Not twoMinusPattern.IsMatch(strNumber)) AndAlso numberPattern.IsMatch(strNumber)
        End Function

        ''' <summary>
        ''' Function To test for Alphabets.
        ''' </summary>
        ''' <param name="strToCheck">String to be checked</param>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public Shared Function IsAlpha(ByVal strToCheck As String) As Boolean
            Dim alphaPattern As Regex = New Regex("[^a-zA-Z]")
            Return Not alphaPattern.IsMatch(strToCheck)
        End Function

        ''' <summary>
        ''' Function to Check for AlphaNumeric.
        ''' </summary>
        ''' <param name="strToCheck">String to be checked</param>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public Shared Function IsAlphaNumeric(ByVal strToCheck As String) As Boolean
            Dim alphaNumericPattern As Regex = New Regex("[^a-zA-Z0-9]")
            Return Not alphaNumericPattern.IsMatch(strToCheck)
        End Function

        ''' <summary>
        ''' Function to check for Email
        ''' </summary>
        ''' <param name="email">Mail detail</param>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public Shared Function IsValidEmail(ByVal email As String) As Boolean
            Dim emailPattern As Regex = New Regex("^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")
            Return Not emailPattern.IsMatch(email)
        End Function
#End Region

#Region "Methods to Execute message"
        ''' <summary>
        ''' To execute sql command
        ''' </summary>
        ''' <param name="message">Class with required details to execute the sql command</param>
        ''' <remarks></remarks>
        Friend Shared Sub Execute(ByVal message As DBMessage)

            Dim dbCommand As DbCommand
            Dim dbFactory As Database = DatabaseFactory.CreateDatabase()
            Dim ds As DataSet
            Dim dr As DbDataReader

            Try
                dbCommand = dbFactory.GetSqlStringCommand(message.InboundData.StoredProcedure)
                dbCommand.CommandType = CommandType.StoredProcedure
                dbCommand.CommandTimeout = 500
                For Each parameter As Parameter In message.InboundData.ParameterList
                    If parameter.ParamDirection = Direction.In Then
                        dbFactory.AddInParameter(dbCommand, parameter.ParamName, parameter.ParamType, parameter.ParamValue)
                    Else
                        dbFactory.AddOutParameter(dbCommand, parameter.ParamName, parameter.ParamType, parameter.ParamSize)
                        message.OutboundData.OutParameters.Add(parameter.ParamName, Nothing)
                    End If
                Next parameter
                If message.InboundData.FetchCountOnly = True Then
                    Dim count As Integer = 0
                    If message.InboundData.Transaction IsNot Nothing Then
                        dr = CType(dbFactory.ExecuteReader(dbCommand, message.InboundData.Transaction), DbDataReader)
                    Else
                        dr = CType(dbFactory.ExecuteReader(dbCommand), DbDataReader)
                    End If
                    If dr.HasRows Then
                        Do While dr.Read()
                            count += 1
                        Loop
                    End If
                    dr.Close()
                    message.OutboundData.Count = count
                    If message.OutboundData.OutParameters.Count > 0 Then
                        Dim keys As List(Of Object) = New List(Of Object)()
                        For Each key As Object In message.OutboundData.OutParameters.Keys
                            keys.Add(key)
                        Next key
                        For Each key As Object In keys
                            message.OutboundData.OutParameters(key) = dbFactory.GetParameterValue(dbCommand, key.ToString())
                        Next key
                    End If
                Else
                    If message.InboundData.Transaction IsNot Nothing Then
                        ds = dbFactory.ExecuteDataSet(dbCommand, message.InboundData.Transaction)
                    Else
                        ds = dbFactory.ExecuteDataSet(dbCommand)
                    End If
                    message.OutboundData.DataSet = ds
                    If message.InboundData.SortColumn <> String.Empty Then 'Sort based on the sort criteria given in the InboundData
                        If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                            Dim dv As DataView = ds.Tables(0).DefaultView
                            Try
                                dv.Sort = message.InboundData.SortColumn
                            Catch
                                'If wrong Column Name is provided, return the default view.
                            End Try
                            message.OutboundData.DataView = dv
                        End If
                    End If
                    If message.OutboundData.OutParameters.Count > 0 Then
                        Dim keys As List(Of Object) = New List(Of Object)()
                        For Each key As Object In message.OutboundData.OutParameters.Keys
                            keys.Add(key)
                        Next key
                        For Each key As Object In keys
                            message.OutboundData.OutParameters(key) = dbFactory.GetParameterValue(dbCommand, key.ToString())
                        Next key
                    End If
                End If
            Catch ex As Exception
                message.Exception = ex
                Throw
            Finally
            End Try
        End Sub
#End Region

#Region "Common"
        ''' <summary>
        ''' To convert sql compatible format.
        ''' </summary>
        ''' <param name="inputSQL"></param>
        ''' <returns></returns>
        ''' <remarks>Formatted String</remarks>
        Public Shared Function SafeSqlLiteral(ByVal inputSQL As String) As String
            Return inputSQL.Replace("'", "''")
        End Function
#End Region
    End Class
End Namespace
