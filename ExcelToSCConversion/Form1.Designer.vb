﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtInfile = New System.Windows.Forms.TextBox
        Me.cmdOpen = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtOutputFile = New System.Windows.Forms.TextBox
        Me.cmdConvert = New System.Windows.Forms.Button
        Me.NSPSelGetAllClientBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StDataSet1 = New ExcelToRVConversion.stDataSet1
        Me.ClientBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StDataSet = New ExcelToRVConversion.stDataSet
        Me.ClientTableAdapter = New ExcelToRVConversion.stDataSetTableAdapters.ClientTableAdapter
        Me.NSP_Sel_GetAllClientTableAdapter = New ExcelToRVConversion.stDataSet1TableAdapters.NSP_Sel_GetAllClientTableAdapter
        Me.pbProgress = New System.Windows.Forms.ProgressBar
        Me.lblProgress = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.rbXMLOutputInvoices = New System.Windows.Forms.RadioButton
        Me.rbXMLOutputExpenses = New System.Windows.Forms.RadioButton
        CType(Me.NSPSelGetAllClientBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(138, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Input Excel File To Convert:"
        '
        'txtInfile
        '
        Me.txtInfile.Location = New System.Drawing.Point(158, 47)
        Me.txtInfile.Name = "txtInfile"
        Me.txtInfile.Size = New System.Drawing.Size(186, 20)
        Me.txtInfile.TabIndex = 1
        '
        'cmdOpen
        '
        Me.cmdOpen.Location = New System.Drawing.Point(353, 46)
        Me.cmdOpen.Name = "cmdOpen"
        Me.cmdOpen.Size = New System.Drawing.Size(71, 21)
        Me.cmdOpen.TabIndex = 2
        Me.cmdOpen.Text = "Open"
        Me.cmdOpen.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Output Filename:"
        '
        'txtOutputFile
        '
        Me.txtOutputFile.Location = New System.Drawing.Point(158, 78)
        Me.txtOutputFile.Name = "txtOutputFile"
        Me.txtOutputFile.Size = New System.Drawing.Size(186, 20)
        Me.txtOutputFile.TabIndex = 4
        '
        'cmdConvert
        '
        Me.cmdConvert.Location = New System.Drawing.Point(353, 79)
        Me.cmdConvert.Name = "cmdConvert"
        Me.cmdConvert.Size = New System.Drawing.Size(71, 21)
        Me.cmdConvert.TabIndex = 5
        Me.cmdConvert.Text = "Convert"
        Me.cmdConvert.UseVisualStyleBackColor = True
        '
        'NSPSelGetAllClientBindingSource
        '
        Me.NSPSelGetAllClientBindingSource.DataMember = "NSP_Sel_GetAllClient"
        Me.NSPSelGetAllClientBindingSource.DataSource = Me.StDataSet1
        '
        'StDataSet1
        '
        Me.StDataSet1.DataSetName = "stDataSet1"
        Me.StDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ClientBindingSource
        '
        Me.ClientBindingSource.DataMember = "Client"
        Me.ClientBindingSource.DataSource = Me.StDataSet
        '
        'StDataSet
        '
        Me.StDataSet.DataSetName = "stDataSet"
        Me.StDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ClientTableAdapter
        '
        Me.ClientTableAdapter.ClearBeforeFill = True
        '
        'NSP_Sel_GetAllClientTableAdapter
        '
        Me.NSP_Sel_GetAllClientTableAdapter.ClearBeforeFill = True
        '
        'pbProgress
        '
        Me.pbProgress.Location = New System.Drawing.Point(160, 145)
        Me.pbProgress.Maximum = 1000
        Me.pbProgress.Name = "pbProgress"
        Me.pbProgress.Size = New System.Drawing.Size(183, 13)
        Me.pbProgress.Step = 1
        Me.pbProgress.TabIndex = 8
        Me.pbProgress.Visible = False
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = True
        Me.lblProgress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProgress.Location = New System.Drawing.Point(95, 145)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(59, 13)
        Me.lblProgress.TabIndex = 9
        Me.lblProgress.Text = "progress:"
        Me.lblProgress.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(155, 149)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(0, 13)
        Me.Label4.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(106, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "XML Output Options:"
        '
        'rbXMLOutputInvoices
        '
        Me.rbXMLOutputInvoices.AutoSize = True
        Me.rbXMLOutputInvoices.Location = New System.Drawing.Point(160, 112)
        Me.rbXMLOutputInvoices.Name = "rbXMLOutputInvoices"
        Me.rbXMLOutputInvoices.Size = New System.Drawing.Size(65, 17)
        Me.rbXMLOutputInvoices.TabIndex = 12
        Me.rbXMLOutputInvoices.TabStop = True
        Me.rbXMLOutputInvoices.Text = "Invoices"
        Me.rbXMLOutputInvoices.UseVisualStyleBackColor = True
        '
        'rbXMLOutputExpenses
        '
        Me.rbXMLOutputExpenses.AutoSize = True
        Me.rbXMLOutputExpenses.Location = New System.Drawing.Point(254, 112)
        Me.rbXMLOutputExpenses.Name = "rbXMLOutputExpenses"
        Me.rbXMLOutputExpenses.Size = New System.Drawing.Size(95, 17)
        Me.rbXMLOutputExpenses.TabIndex = 13
        Me.rbXMLOutputExpenses.TabStop = True
        Me.rbXMLOutputExpenses.Text = "Expenses Only"
        Me.rbXMLOutputExpenses.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(436, 189)
        Me.Controls.Add(Me.rbXMLOutputExpenses)
        Me.Controls.Add(Me.rbXMLOutputInvoices)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.pbProgress)
        Me.Controls.Add(Me.cmdConvert)
        Me.Controls.Add(Me.txtOutputFile)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmdOpen)
        Me.Controls.Add(Me.txtInfile)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Excel to RV XML Conversion"
        CType(Me.NSPSelGetAllClientBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtInfile As System.Windows.Forms.TextBox
    Friend WithEvents cmdOpen As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtOutputFile As System.Windows.Forms.TextBox
    Friend WithEvents cmdConvert As System.Windows.Forms.Button
    Friend WithEvents StDataSet As ExcelToRVConversion.stDataSet
    Friend WithEvents ClientBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ClientTableAdapter As ExcelToRVConversion.stDataSetTableAdapters.ClientTableAdapter
    Friend WithEvents StDataSet1 As ExcelToRVConversion.stDataSet1
    Friend WithEvents NSPSelGetAllClientBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NSP_Sel_GetAllClientTableAdapter As ExcelToRVConversion.stDataSet1TableAdapters.NSP_Sel_GetAllClientTableAdapter
    Friend WithEvents pbProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents rbXMLOutputInvoices As System.Windows.Forms.RadioButton
    Friend WithEvents rbXMLOutputExpenses As System.Windows.Forms.RadioButton

End Class
