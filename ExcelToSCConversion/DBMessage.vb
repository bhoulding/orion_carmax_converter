Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Reflection
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Namespace OrionUtility
    <Serializable()> _
    Public Class DBMessage
#Region "Member Variables"
        Private _inboundData As InBoundData = Nothing
        Private _outboundData As OutBoundData = Nothing
        Private _exception As Exception = Nothing
#End Region

#Region "Constructor"
        Public Sub New()
            Me._inboundData = New InBoundData()
            Me._outboundData = New OutBoundData()
        End Sub
#End Region

#Region "Property Procedures"
        Public ReadOnly Property InboundData() As InBoundData
            Get
                Return Me._inboundData
            End Get
        End Property

        Public ReadOnly Property OutboundData() As OutBoundData
            Get
                Return Me._outboundData
            End Get
        End Property

        Public Property Exception() As Exception
            Get
                Return Me._exception
            End Get
            Set(ByVal value As Exception)
                Me._exception = value
            End Set
        End Property
#End Region
    End Class
    <Serializable()> _
    Public Class InBoundData
#Region "Member Variables"
        Private _storedProcedure As String = Nothing
        Private _parameterList As List(Of Parameter) = New List(Of Parameter)()
        Private _fetchCountOnly As Boolean = False
        Private _transaction As DbTransaction = Nothing
        Private _sortColumn As String = String.Empty
#End Region

#Region "Constructors"
        Public Sub New()
        End Sub
#End Region

#Region "Public Methods"
        Public Sub AddInParameter(ByVal paramName As String, ByVal paramType As DbType, ByVal paramValue As Object)
            Dim parameter As Parameter = New Parameter(paramName, paramType, paramValue, Direction.In)
            Me._parameterList.Add(parameter)
        End Sub
        Public Sub AddInParameter(ByVal paramName As String, ByVal paramType As DbType, ByVal paramSize As Integer, ByVal paramValue As Object)
            Dim parameter As Parameter = New Parameter(paramName, paramType, paramSize, paramValue, Direction.In)
            Me._parameterList.Add(parameter)
        End Sub
        Public Sub AddOutParameter(ByVal paramName As String, ByVal paramType As DbType, ByVal paramSize As Integer)
            Dim parameter As Parameter = New Parameter(paramName, paramType, paramSize, Direction.Out)
            Me._parameterList.Add(parameter)
        End Sub
#End Region

#Region "Property Procedures"
        Public Property StoredProcedure() As String
            Get
                Return Me._storedProcedure
            End Get
            Set(ByVal value As String)
                Me._storedProcedure = value
            End Set
        End Property
        Public ReadOnly Property ParameterList() As List(Of Parameter)
            Get
                Return Me._parameterList
            End Get
        End Property
        Public Property FetchCountOnly() As Boolean
            Get
                Return Me._fetchCountOnly
            End Get
            Set(ByVal value As Boolean)
                Me._fetchCountOnly = value
            End Set
        End Property
        Public Property Transaction() As DbTransaction
            Get
                Return Me._transaction
            End Get
            Set(ByVal value As DbTransaction)
                Me._transaction = value
            End Set
        End Property

        Public Property SortColumn() As String
            Get
                Return Me._sortColumn
            End Get
            Set(ByVal value As String)
                Me._sortColumn = value
            End Set
        End Property
#End Region
    End Class

    <Serializable()> _
    Public Class OutBoundData
#Region "Member Variables"
        Private _dataSet As DataSet = Nothing
        Private _dataView As DataView = Nothing
        Private _outParameterList As Hashtable = New Hashtable()
        Private _count As Integer = 0
#End Region

#Region "Constructor"
        Public Sub New()
        End Sub
#End Region

#Region "Property Procedures"
        Public Property DataSet() As DataSet
            Get
                Return Me._dataSet
            End Get
            Set(ByVal value As DataSet)
                Me._dataSet = value
            End Set
        End Property

        Public Property DataView() As DataView
            Get
                Return Me._dataView
            End Get
            Set(ByVal value As DataView)
                Me._dataView = value
            End Set
        End Property

        Public Property OutParameters() As Hashtable
            Get
                Return Me._outParameterList
            End Get
            Set(ByVal value As Hashtable)
                Me._outParameterList = value
            End Set
        End Property
        Public Property Count() As Integer
            Get
                Return Me._count
            End Get
            Set(ByVal value As Integer)
                Me._count = value
            End Set
        End Property
#End Region
    End Class

    <Serializable()> _
    Public Class Parameter
#Region "Member Variables"
        Private _paramName As String = String.Empty
        Private _paramValue As Object = Nothing
        Private _paramSize As Integer = Integer.MinValue
        Private _paramType As DbType
        Private _paramDirection As Direction
#End Region
#Region "Constructor"
        Public Sub New(ByVal paramName As String, ByVal paramType As DbType, ByVal paramValue As Object, ByVal paramDirection As Direction)
            Me._paramName = paramName
            Me._paramType = paramType
            Me._paramValue = paramValue
            Me._paramDirection = paramDirection
        End Sub
        Public Sub New(ByVal paramName As String, ByVal paramType As DbType, ByVal paramSize As Integer, ByVal paramValue As Object, ByVal paramDirection As Direction)
            Me._paramName = paramName
            Me._paramType = paramType
            Me._paramSize = paramSize
            Me._paramValue = paramValue
            Me._paramDirection = paramDirection
        End Sub
        Public Sub New(ByVal paramName As String, ByVal paramType As DbType, ByVal paramSize As Integer, ByVal paramDirection As Direction)
            Me._paramName = paramName
            Me._paramType = paramType
            Me._paramSize = paramSize
            Me._paramDirection = paramDirection
        End Sub
#End Region
#Region "Property Procedures"
        Public Property ParamName() As String
            Get
                Return Me._paramName
            End Get
            Set(ByVal value As String)
                Me._paramName = value
            End Set
        End Property
        Public Property ParamValue() As Object
            Get
                Return Me._paramValue
            End Get
            Set(ByVal value As Object)
                Me._paramValue = value
            End Set
        End Property
        Public Property ParamSize() As Integer
            Get
                Return Me._paramSize
            End Get
            Set(ByVal value As Integer)
                Me._paramSize = value
            End Set
        End Property
        Public Property ParamType() As DbType
            Get
                Return Me._paramType
            End Get
            Set(ByVal value As DbType)
                Me._paramType = value
            End Set
        End Property
        Public Property ParamDirection() As Direction
            Get
                Return Me._paramDirection
            End Get
            Set(ByVal value As Direction)
                Me._paramDirection = value
            End Set
        End Property
#End Region
    End Class
End Namespace
